import React from 'react';
import { Button, Col, Form, Row } from 'react-bootstrap';
import TrainAxios from '../../apis/TrainAxios';
import { withRouter } from './../../withRouter';

class Registration extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userRegistrationDto: {
        username: "",
        eMail: "",
        name: "",
        surname: "",
        password: "",
        repeatedPassword: ""
      }
    };

    this.valueInputChange = this.valueInputChange.bind(this);
    this.register = this.register.bind(this);
  }

  valueInputChange(e) {
    const { name, value } = e.target;

    this.setState(prevState => ({
      userRegistrationDto: {
        ...prevState.userRegistrationDto,
        [name]: value
      }
    }));
  }

  register(e) {
    e.preventDefault();

    const { userRegistrationDto } = this.state;

    TrainAxios.post('/users', userRegistrationDto)
      .then(res => {
        console.log(res);
        alert('Successfully registered');
        this.props.navigate('/login');
      })
      .catch(error => {
        console.error(error.response || error.message);
        alert('Error with registration. Please try again.');
      });
  }

  render() {
    const { userRegistrationDto } = this.state;

    return (
      <>
        <Row>
          <Col></Col>
          <Col xs="14" sm="12" md="12" className="font-weight-bold">
            <Form>
              <Form.Group>
                <Form.Label htmlFor="username">Username</Form.Label>
                <Form.Control
                  placeholder="Your username"
                  id="username"
                  name="username"
                  autoComplete="username"
                  value={userRegistrationDto.username}
                  onChange={this.valueInputChange}
                  required
                />
              </Form.Group>

              <Form.Group>
                <Form.Label htmlFor="eMail">Email</Form.Label>
                <Form.Control
                  placeholder="Your email"
                  id="eMail"
                  name="eMail"
                  type="email"
                  autoComplete="email"
                  value={userRegistrationDto.eMail}
                  onChange={this.valueInputChange}
                  required
                />
              </Form.Group>

              <Form.Group>
                <Form.Label htmlFor="name">Name</Form.Label>
                <Form.Control
                  placeholder="Your name"
                  id="name"
                  name="name"
                  autoComplete="given-name"
                  value={userRegistrationDto.name}
                  onChange={this.valueInputChange}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label htmlFor="surname">Surname</Form.Label>
                <Form.Control
                  placeholder="Your last name"
                  id="surname"
                  name="surname"
                  autoComplete="family-name"
                  value={userRegistrationDto.surname}
                  onChange={this.valueInputChange}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label htmlFor="password">Password</Form.Label>
                <Form.Control
                  placeholder="Create your password"
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="new-password"
                  value={userRegistrationDto.password}
                  onChange={this.valueInputChange}
                  required
                />
              </Form.Group>

              <Form.Group>
                <Form.Label htmlFor="repeatedPassword">Repeat Password</Form.Label>
                <Form.Control
                  placeholder="Repeat your password"
                  id="repeatedPassword"
                  name="repeatedPassword"
                  type="password"
                  autoComplete="new-password"
                  value={userRegistrationDto.repeatedPassword}
                  onChange={this.valueInputChange}
                  required
                />
              </Form.Group>

              <Button
                style={{ marginTop: "25px" }}
                onClick={this.register}
              >
                Register
              </Button>
            </Form>
          </Col>
          <Col></Col>
        </Row>
      </>
    );
  }
}

export default withRouter(Registration);