import React from 'react';
import TrainAxios from '../../apis/TrainAxios';
import {Button, Col, Form, InputGroup, Row} from 'react-bootstrap';
import {withRouter} from './../../withRouter';

class EditLine extends React.Component {

    constructor(props){
    super(props);

    let line={
        id: -1,
        numberOfSeats: 0,
        ticketPrice: 0.0,
        time: "",
        destination: "",
        train: {}
    }

    this.state={line:line, trains:[]};

    }

    componentDidMount(){
        this.getLineById(this.props.params.id);
        this.getTrains();
    }

    getLineById(id){
        TrainAxios.get('/lines/' + id)
        .then(res=>{
            console.log(res.data);
            let line = {
                id: res.data.id,
                numberOfSeats: res.data.numberOfSeats,
                ticketPrice: res.data.ticketPrice,
                time: res.data.time,
                destination: res.data.destination,
                train: res.data.train
            }
            this.setState({line: line});
        })
        .catch(error=>{
            console.log(error);
            alert('problem with getting line')
        });
    }

    getTrains(){
        TrainAxios.get('/trains')
        .then(res=>{
            console.log(res)
            this.setState({trains: res.data})
        })
        .catch(error=>{
            console.log(error);
            alert('problem with getting trains')
        });
    }

    valueInputChange(e){
        let input = e.target;

        let name = input.name;
        let value = input.value;

        let line = this.state.line;
        line[name] = value;
        this.setState({line: line});
    }

    trainSelectionChange(e){
        let trainId = e.target.value;
        let train = this.state.trains.find((train)=> train.id = trainId);
        console.log(train);

        let line = this.state.line;
        line.train = train;

        this.setState({line:line});
        

    }

    renderTrainsOptions(){
        return this.state.trains.map(train=>{
            return(
                <option key={train.id} value={train.id}>
                    {train.name}
                </option>
            )
        })
    }

    edit(){
        let line = this.state.line;
        let lineDTO = {
            id: line.id,
            numberOfSeats: line.numberOfSeats,
            ticketPrice: line.ticketPrice,
            time: line.time,
            destination: line.destination,
            train: line.train
        }
        TrainAxios.put('/lines/' + this.state.line.id, lineDTO)
        .then(res=>{
            alert('successifuly changed line')
            this.props.navigate('/lines');
            
        })
        .catch(error=>{
            console.log(error);
            alert('unseccessiful change, try again')
        })

    }




    render(){
        return(
            <>
                <Row>
                <Col></Col>
                <Col xs="14" sm="12" md="12" className="font-weight-bold">
                    
                <h1>Edit train Line</h1>
                <Form>
                    <Form.Label htmlFor="numberOfSeats">Number of Seats</Form.Label>
                    <Form.Control type="number" id="numberOfSeats" name="numberOfSeats" onChange={(e) => this.valueInputChange(e)}/> <br/>
                    <Form.Label htmlFor="ticketPrice" >Ticket price</Form.Label>
                    <Form.Control type="number" id="ticketPrice" name="ticketPrice" onChange={(e)=>this.valueInputChange(e)}/> <br/>
                    <Form.Label id="time">Time of depature</Form.Label>
                    <Form.Control id="time" name="time" type="time" onChange={(e)=>this.valueInputChange(e)}/> <br/>
                    <Form.Label htmlFor="destination" >Destination</Form.Label>
                    <Form.Control id="destination" name="destination" onChange={(e)=>this.valueInputChange(e)}/> <br/>
    
                    <Form.Label htmlFor="train">Train</Form.Label>
                    <InputGroup>
                    <Form.Control as="select" id="train" name="train" value={this.state.line.train.id} onChange={(e)=>this.trainSelectionChange(e)}>
                    {this.renderTrainsOptions()}
                    </Form.Control>
    
                    </InputGroup>
    
                    <Button style={{ marginTop: "25px" }} variant="warning" onClick={() => this.edit()}>Edit</Button>
                </Form>
                </Col>
    
                
                <Col></Col>
                </Row>
    
        
              
         
            </>
         
        )
    }
}
export default withRouter(EditLine);