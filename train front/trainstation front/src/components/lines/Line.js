import React from 'react';
import TrainAxios from './../../apis/TrainAxios';
import {Button, ButtonGroup, Col, Collapse, Form, InputGroup, Table} from 'react-bootstrap'
import {withRouter} from './../../withRouter';


class Line extends React.Component{

    constructor(props){
        super(props);

        this.state = {lines: [], trains: [],
        search: {destination:"", trainId: -1, ticketPriceTo: 0.00},
        showSearch: false, pageNo:0, totalPages: 0}
    }

    componentDidMount(){
        this.getLines(0);
        this.getTrains();
    }



    getLines(pageNo){

        let config = {
            params:{
                pageNo: pageNo,
                
            }
        }

        TrainAxios.get('/lines', config)
        .then(res=>{
            this.setState({
                lines: res.data,
                pageNo: pageNo, totalPages: res.headers['total-pages']
            });
            console.log(res);
        })
        .catch(error=>{
            console.log(error);
            alert('error with getting lines')
        });
    
    }

    getTrains(){
        TrainAxios.get('/trains')
        .then(res=>{
            this.setState({trains: res.data})
            console.log(res);
        })
        .catch(error=>{
            console.log(error);
            alert('problems with trains');
        })
    }

    goToAdd(){
        this.props.navigate('/lines/add');
    }

    goToEdit(id){
        this.props.navigate('/lines/edit/' + id);
    }

    goToDelete(id){
        TrainAxios.delete('/lines/' + id)
        .then(res=>{
            console.log('Delete success:', res)
            alert('successifuly deleted line')
            window.location.reload()

        })
        .catch(error=>{
            console.log('Delete error',error);

            if (error.response) {
                // Backend response received but with an error status
                console.error('Response Data:', error.response.data);
                console.error('Response Status:', error.response.status);
                alert(`Failed to delete line: ${error.response.data.message || 'Forbidden'}`);
            } else if (error.request) {
                // No response received
                console.error('Request Error:', error.request);
                alert('No response received from server.');
            } else {
                // Other errors
                console.error('Error:', error.message);
                alert('An error occurred.');
            }
            alert('line was not deleted')
        });
    }

    goToReserve(id){
        this.props.navigate('/reservation/' + id);
    }

    
    

    renderLines(){
        return this.state.lines.map((line)=> {
            return(
                <tr key={line.id}>
                    <td className="table-primary text-center font-weight-bold">{line.train.name}</td>
                    <td className="table-primary text-center font-weight-bold">{line.train.type}</td>
                    <td className="table-primary text-center font-weight-bold">{line.destination}</td>
                    <td className="table-primary text-center font-weight-bold">{line.numberOfSeats}</td>
                    <td className="table-primary text-center font-weight-bold">{line.time}</td>
                    <td className="table-primary text-center font-weight-bold">{line.ticketPrice}</td>

                    {window.localStorage['role']==='ROLE_ADMIN' ? (
                        <>
                            <td key={`edit-${line.id}`}><Button style={{margin:5}} variant="warning" onClick={()=> this.goToEdit(line.id)}>Edit line</Button></td>,
                            <td key={`delete-${line.id}`}><Button style={{margin:5}} variant="danger" onClick={()=> this.goToDelete(line.id)}>Delete line</Button></td>) 
                        </>
                    ) : null}

                    <td>
                        <Button disabled={line.numberOfSeats==0} onClick={()=>this.goToReserve(line.id)}>Ticket reservation</Button>
                    </td>
                
                </tr>
            )
        })
    }


    searchValueChange(e) {
    const { name, value } = e.target;

    this.setState(prevState => ({
        search: {
            ...prevState.search,
            [name]: value
        }
    }));
    console.log(this.state.search);
}
    

    renderTrainsOptions(){
        return this.state.trains.map((train) => {
            return(
                <option key={train.id} value={train.id}>
                    {train.name}
                </option>
            );
        });
    }

    search(){

        console.log(localStorage.getItem('token'));  // Check if the token exists
        
        let config = {params:{}};

            if(this.state.search.destination){
            config.params.destination = this.state.search.destination;
            }
            if(this.state.search.trainId !== -1){
            config.params.trainId = this.state.search.trainId;
            }
            if(this.state.search.ticketPriceTo !== 0.00){
            config.params.ticketPriceTo = this.state.search.ticketPriceTo;
        }

        console.log(config);

        TrainAxios.get('/lines/search',{
            headers: { 
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            params: config.params
        })
            .then(res => {
                console.log('Search response', res);
                this.setState({lines: res.data});
            })
            .catch(error => {
                console.log('Search error',error);


                if (error.response) {
                    // The server responded with a status code outside the range of 2xx
                    console.error('Response Data:', error.response.data);
                    console.error('Response Status:', error.response.status);
                    console.error('Response Headers:', error.response.headers);
                } else if (error.request) {
                    // No response was received
                    console.error('Request Error:', error.request);
                } else {
                    // Something else happened
                    console.error('General Error:', error.message);
                }

                alert('problems with getting lines');
            });
        
    }

    render(){
        return(
            <div>
                <div className="font-weight-bold">
                    <h2>Search train lines</h2>
        
                    <Col></Col>
                    <Col xs="14" sm="12" md="12">
                    <Form.Group style={{marginTop:35}}>
                    <Form.Check type="checkbox" label="Show search form" onClick={(event) => this.setState({showSearch: event.target.checked})}/>
                    </Form.Group>

                     <Collapse in={this.state.showSearch}>
                     <Form>
                        <Form.Label htmlFor="destination">Destination</Form.Label>
                        <Form.Control id="destination" name="destination" placeholder="write your destination" onChange={(e)=>this.searchValueChange(e)}/> <br/>

                
                        <Form.Label htmlFor="trainId">Train</Form.Label>
                        <InputGroup>
                        <Form.Control as="select" id="trainId" name="trainId" onChange={(e)=>this.searchValueChange(e)}> 
                     <option>Choose train</option>
                     {this.renderTrainsOptions()}
                
                        </Form.Control>
                     </InputGroup>
                        <br/>
                     <Form.Label htmlFor="ticketPriceTo">Maximum ticket price</Form.Label>
                     <Form.Control placeholder="Write maximum ticket price" id="ticketPriceTo" name="ticketPriceTo" onChange={(e)=>this.searchValueChange(e)}/> <br/>
                
                    
                     <Button style={{ marginTop: "25px" }}onClick={() => this.search()}>Search</Button>
                     </Form>
                     </Collapse>
                    </Col>
                </div>

                <div><br/>
                    <h1>Train lines</h1>
                    <br/>
                    <div style={{float:"left"}}>
                    {window.localStorage['role']==='ROLE_ADMIN' ?
                    [<Button variant="success" onClick={()=>this.goToAdd()}>Create new line</Button>] : null}
                    </div>
                    <br/>
                 <br/>

                 <ButtonGroup style={{ marginTop: 25, float:"right"}}>
                 <Button style={{ margin: 3, width: 90}} variant="info" disabled={this.state.pageNo==0} onClick={()=>this.getLines(this.state.pageNo-1)}>
                    Previous 
                 </Button>
                 <Button style={{ margin: 3, width: 90}} variant="info" disabled={this.state.pageNo==this.state.totalPages-1} onClick={()=>this.getLines(this.state.pageNo+1)}>
                  Next
                 </Button>
                 </ButtonGroup>

                    <Table className="table table-striped" style={{marginTop:10}}>
                     <thead className="thead-dark text-center">
                        <tr>
                            <th>Train name</th>
                            <th>Train type</th>
                            <th>Destination</th>
                            <th>Number of seats</th>
                            <th>Depature Time</th>
                            <th>Ticket price</th>
                            <th colSpan={10}>Options</th>

                        </tr>
                     </thead>
                        <tbody>
                        {this.renderLines()}
                        </tbody>
                    </Table>
                </div>
            </div>
        );
    }
}
export default withRouter(Line);