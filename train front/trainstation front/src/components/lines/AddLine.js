import React from 'react';
import {Button, Col, Form, InputGroup, Row} from 'react-bootstrap';
import TrainAxios from '../../apis/TrainAxios';
import {withRouter} from './../../withRouter';

class AddLine extends React.Component{

    constructor(props){
        super(props);

        let line={
            numberOfSeats: 0,
            ticketPrice: 0.0,
            time: "",
            destination: "",
            train: null
        }

        this.state={line: line, trains: []};
    
    }

    componentDidMount(){
        this.getTrains();
    }

    getTrains(){
        TrainAxios.get('/trains')
        .then(res=> {
            console.log(res);
            this.setState({trains: res.data});
        })
        .catch(error=>{
            console.log(error);
            alert('error with getting trains')
        })
    }

    valueInputChange(e){
        let input = e.target;

        let name = input.name;
        let value = input.value;

        let line = this.state.line;
        line[name] = value;

        this.setState({line: line});
        console.log(this.state.line);
    }

    trainSelectionChange(e){
        let trainId = e.target.value;
        let train = this.state.trains.find((train)=> train.id == trainId);
        console.log(train);

        let line = this.state.line;
        line.train = train;

        this.setState({line: line});
    }

    renderTrainsOptions(){
        return this.state.trains.map((train)=>{
            return(
                <option key={train.id} value={train.id}>
                    {train.name}
                </option>
            )
        });
    }

    create(e){
        e.preventDefault();

        let line = this.state.line;
        let lineDTO = {
            numberOfSeats: line.numberOfSeats,
            ticketPrice: line.ticketPrice,
            time: line.time,
            destination: line.destination,
            train: line.train
        }

        TrainAxios.post('/lines', lineDTO)
        .then(res=>{
            console.log(res);
            alert('succesifuly created new line');
            this.props.navigate('/lines');
            
        })
        .catch(error=>{
            console.log(error);
            alert('problems with getting lines');
        })

    }

    render(){
        return(
            <>
            <h1>Creating new line</h1>
            <Row>
            <Col></Col>
            <Col xs="16" sm="12" md="16">
            <Form>
                <Form.Label htmlFor="numberOfSeats"><div className="p-2 mb-2 bg-success text-white">Number of seats</div></Form.Label>
                <Form.Control type="number" id="numberOfSeats" name="numberOfSeats" onChange={(e)=>this.valueInputChange(e)}/> <br/>
                <Form.Label htmlFor="ticketPrice"><div className="p-2 mb-2 bg-success text-white">Ticket price</div></Form.Label>
                <Form.Control id="ticketPrice" name="ticketPrice" onChange={(e)=>this.valueInputChange(e)}/> <br/>
                <Form.Label id="Destination"><div className="p-2 mb-2 bg-success text-white">Destination</div></Form.Label>
                <Form.Control  id="Destination" name="destination" onChange={(e)=>this.valueInputChange(e)}/> <br/>
                <Form.Label id="time"><div className="p-2 mb-2 bg-success text-white">Time of depature</div></Form.Label>
                <Form.Control  id="time" name="time" type="time" onChange={(e)=>this.valueInputChange(e)}/> <br/>
    
                <Form.Label htmlFor="train"><div className="p-2 mb-2 bg-success text-white">Train</div></Form.Label>
                <InputGroup>
                <Form.Control as="select" id="train" name="train" placeholder="train" onChange={(e)=>this.trainSelectionChange(e)}>
                <option>Choose train</option>
                {this.renderTrainsOptions()}
                </Form.Control>
    
                </InputGroup>
               
                <Button variant="success" style={{marginTop: "25px"}}onClick={(e) => this.create(e)}>Create</Button>
            </Form>
            </Col>
            <Col></Col>
            </Row>
            
        </>
            
        )
    }
}
export default withRouter(AddLine);