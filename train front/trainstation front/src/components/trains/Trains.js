import React from 'react';


class Trains extends React.Component{

 
    render(){
        return(
            <div>
                <h1>Trains</h1>
              
                <div className="row">
			
			    <div className="w-col col bg-light  text-black" >
				    <h4>Soko</h4>
				    Soko train is an intercity type of train. For now,
                    it operates between three cities 
				    in Serbia: Novi Sad, Beograd and 
                    Subotica. It represents the fastest type of 
                    train in Serbia and reaches a speed of 200 km 
                    per hour.In the future, this type of train should
                    also operate outside the borders of the country.
                    This is the first high-speed train in the history 
                    of Serbian railways, and also the most modern in the 
                    region.
                    
			    </div>
                <br/>
                

                <div className = "w-col col bg-secondary text-black">
				    <h4>SerbiaTrain</h4>
				SerbiaTrain is an international type of train. This 
                train travels in countries all over Europe, for some 
                connections with transfers in major European cities.
                The fleet of passenger cars has 797 trains. Traction 
                vehicles consist of electric and diesel locomotives 
                and motor trains.
                
			    </div>
                <br/>

                <div className = "w-col col bg-light text-black">
				    <h4>RegioTrain</h4>
				RegioTrain is a type of train that operates in Serbia.
                The characteristic of these trains is not the speed 
                because they have many way stations on their routes.
                In addition to the comfort of the seats, there is the 
                possibility to spend the trip in a sleeping car. 
                For even greater comfort, it is possible to load your 
                car onto the train.
                
			    </div>
                <br/>

                <div className = "w-col col bg-secondary text-black">
				    <h4>Nostalgic</h4>
				The tourist-museum train "Nostalgijsa" with wagons from the 20s
                and 30s of the 20th century operates on the route Mokra Gora - Visegrad.
                A part of this railway on the route from Šargan Vitasi station to Mokra Gora
                was torn from oblivion by the famous "Šargan eight".
                On this route, you pass through 22 tunnels, over five bridges, and overcome
                a height difference of 300 meters.During the ride, passengers have the opportunity
                to see the wealth and beauty of the nature of this area from one of the five viewpoints.

			    </div>
		
			
			
		    </div>
                
            
            </div>
        )
    }
}
export default Trains;