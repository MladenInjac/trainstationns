import React from 'react';
import {Button, Col, Form, Row} from 'react-bootstrap';
import TrainAxios from './../../apis/TrainAxios';
import {withRouter} from './../../withRouter';

class Reservation extends React.Component{

    constructor(props){
        super(props);

        let line = {
            id: -1,
            numberOfSeats: 0,
            ticketPrice: 0.0,
            time: "",
            destination: "",
            train: []
        }

        this.state={numberOfPassengers: 0, date: "", onName: "",
         reservations:[], line,user: {}};

    }

    componentDidMount(){
        this.getLineById(this.props.params.id);
        this.setUser();
    }

    setUser(){
        this.getUserByUsername(window.localStorage['username']);
    }


    getUserByUsername(username){
        TrainAxios.get('/users/' + username)
        .then(res=>{
            console.log(res.data);
            let user = {
                id: res.data.id,
                username: res.data.username,
                eMail: res.data.eMail,
                name: res.data.name,
                surname: res.data.surname,
                reservations: res.data.reservations
            }
            this.setState({reservations: res.data.reservations});
            this.setState({user: user});

        })
        .catch(error=>{
            console.log(error);
            alert('You have to login to reserve a ticket')
        });
    }

    getLineById(lineId){
        TrainAxios.get('/lines/' + lineId)
        .then(res=>{
            console.log(res);
            let line = {
                id: res.data.id,
                numberOfSeats: res.data.numberOfSeats,
                ticketPrice: res.data.ticketPrice,
                time: res.data.time,
                destination: res.data.destination,
                train: res.data.train
            }
            this.setState({line:line});
        })
        .catch(error=>{
            console.log(error);
            alert('problem with getting line')
        })
    }

    reserve(e){
        e.preventDefault();

        let reservationDTO = {
            numberOfPassengers: this.state.numberOfPassengers,
            onName: this.state.onName,
            date: this.state.date,
            line: this.state.line,
            user: this.state.user
        }

        TrainAxios.post('/reservations', reservationDTO)
        .then(res=>{
            console.log(res);
            alert('succesifuly reserved ticket')
            this.props.navigate('/lines');
        })
        .catch(error=>{
            console.log(error);
            alert('problems with reservation')
        });
    }



    numOfPassengersInputChange(e){
       let input = e.target;
       let name = input.name;
       let value = input.value;
       console.log(name + "," + value);

       let numberOfPassengers = this.state.numberOfPassengers;
       numberOfPassengers = value;

       this.setState({numberOfPassengers: numberOfPassengers});
        
    }

    onNameInputChange(e){
        let input = e.target;
        let name = input.name;
        let value = input.value;
        console.log(name + "," + value);

        let onName = this.state.onName;
        onName = value;

        this.setState({onName: onName});
    }

    dateInputChange(e){
        let input = e.target;
        let name = input.name;
        let value = input.value;
        console.log(name + "," + value);

        let date = this.state.date;

        date = value;

        this.setState({date:date});
    }


  




    render(){
        return (
            
           
            <>
             
            <Row>
                
                <h1>You choose destination : {this.state.line.destination}. Depature time is: {this.state.line.time}h</h1>
                <h1> <div className="p-2 mb-2 bg-danger text-white">Free seats aveilable : {this.state.line.numberOfSeats}</div></h1>
                
            <Col></Col>
            <Col xs="14" sm="12" md="12" className="font-weight-bold">
            <Form>
                <Form.Label id="onName">Name</Form.Label>
                <Form.Control placeholder="Write name for reservation" id="onName" name="onName" onChange={(e)=>this.onNameInputChange(e)}/> <br/>
                <Form.Label id="numberOfPassengers" size="50px">Number of passengers</Form.Label>
                <Form.Control type="number" placeholder="Select number of passengers for your reservation" id="numberOfPassengers" name="numberOfPassengers" onChange={(e)=>this.numOfPassengersInputChange(e)}/> <br/>

                <Form.Group>
                 <Form.Label id="Date">Date</Form.Label>
                <Form.Control type="date" placeholder="Select date for your reservation" id="Date" name="Date" onChange={(e)=>this.dateInputChange(e)}/> <br/>
                </Form.Group>
                <br/>
                

               
                <Button style={{ marginTop: "25px" }}onClick={(e) => this.reserve(e)}>Book a ticket</Button>
                
            </Form>
            </Col>
    
            <Col></Col>
            </Row>
         
        </>

        )
    }
}
export default withRouter(Reservation);