import React from 'react';
import TrainAxios from '../../apis/TrainAxios';
import {Button, Table} from 'react-bootstrap';

class UserReservations extends React.Component{

    constructor(props){
        super(props);

        let user = {
            id: -1,
            username: "",
            eMail: "",
            name: "",
            surname: "",
            reservations: []
        }


        this.state={ reservations: [], user:user};
    }

    componentDidMount(){
        this.setUser();
    }

    setUser(){
        this.getUserByUsername(window.localStorage['username']);
    }

    getUserByUsername(username){
        TrainAxios.get('/users/' + username)
        .then(res=>{
            console.log(res.data);
            let user = {
                id: res.data.id,
                username: res.data.username,
                eMail: res.data.eMail,
                name: res.data.name,
                surname: res.data.surname,
                reservations: res.data.reservations
            }
            this.setState({reservations: user.reservations});
            this.setState({user: user});

            console.log(res.data.reservations);

        })
        .catch(error=>{
            console.log(error);
            alert('Problems with getting user data')
        });
    }

    delete(id){

        TrainAxios.delete('/reservations/' + id )
        .then(res=> {
            console.log(res);
            alert('You successifuly deleted your reservation');
            window.location.reload();
            
        })
        .catch(error => {
            console.log(error);
            alert('Error with deleting reservation');
        });
    }


    renderReservations() {
        return this.state.reservations.map((reservation) => {
            return (
               <tr key={reservation.id}>
                  <td>{reservation.numberOfPassengers}</td>
                   <td>{reservation.line.destination}</td>
                  <td>{reservation.onName}</td>
                  <td>{reservation.date}</td>
                  <td>{reservation.line.time}</td>
                  <td>
                    <Button variant="danger" onClick={()=>this.delete(reservation.id)}>Cancel reservation</Button>
                  </td>
                 
      
        
                  
                  
                  
               </tr>
            )
         })
    }





    render(){
        
        return (
            <div>
                
                <div>
                   
                    <h1>Reservations of user : {window.localStorage['username']}</h1>
                    
                    
                    <Table className="table table-dark text-center" style={{marginTop:5}}>
                        <thead className="thead-dark text-center">
                            <tr>
                                <th>Number of Passegers</th>
                                <th>Line destination</th>
                                <th>Reservation on Name: </th>
                                <th>Reservation date</th>
                                <th>Depature time</th>
                                <th></th>
                                
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderReservations()}
                        </tbody>                  
                    </Table>
                    
                    
                </div>
                
    
            </div>
        );
      
    }
}
export default UserReservations;