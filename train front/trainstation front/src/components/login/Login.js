import React from 'react';
import { Button, Col, Form, Row } from 'react-bootstrap';
import { login } from '../../services/auth';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: ""
    };

    // Bind the onInputChange method
    this.onInputChange = this.onInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  onInputChange(e) {
    const { name, value } = e.target; // Destructure name and value from the event target
    this.setState({ [name]: value }); // Use computed property names for cleaner code
  }

  handleSubmit(e) {
    e.preventDefault(); // Prevent form from reloading the page
    const { username, password } = this.state;
    // Call login function when form is submitted
    login(username, password);
  }

  render() {
    return (
      <Row className="justify-content-center font-weight-bold">
        <Col md={6}>
          <Form onSubmit={this.handleSubmit}> 
            {/* Username Field */}
            <Form.Group>
              <Form.Label htmlFor="username">Username</Form.Label>
              <Form.Control
                id="username"
                placeholder="Write your username"
                type="text"
                name="username"
                autoComplete="username"
                onChange={this.onInputChange}
              />
            </Form.Group>

            {/* Password Field */}
            <Form.Group>
              <Form.Label htmlFor="password">Password</Form.Label>
              <Form.Control
                id="password"
                placeholder="Write your password"
                type="password"
                name="password"
                autoComplete="current-password"
                onChange={this.onInputChange}
              />
            </Form.Group>

            <br />

            {/* Login Button */}
            <Button
              variant="success"
              type="submit"
            >
              Log in
            </Button>
          </Form>
        </Col>
      </Row>
    );
  }
}

export default Login;