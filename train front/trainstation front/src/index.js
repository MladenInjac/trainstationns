import React from 'react';
import ReactDOM from 'react-dom/client';
import {HashRouter as Router, Link, Route, Routes} from 'react-router-dom';
import {Button, Container, Nav, Navbar} from 'react-bootstrap';
import Home from './components/Home';
import NotFound from './NotFound';
import Line from './components/lines/Line';
import './index.css'
import AddLine from './components/lines/AddLine';
import EditLine from './components/lines/EditLine';
import Reservation from './components/reservations/Reservation';
import Login from './components/login/Login';
import {logout} from './services/auth';
import Registration from './components/registration/Registration';
import UserReservations from './components/reservations/UserReservations';
import Trains from './components/trains/Trains';


class App extends React.Component{

  render(){
    return(
      <div>
        
        <Router>
          <Navbar expand bg= "dark" variant="dark">
            <Navbar.Brand as={Link} to="/">
              Home
            </Navbar.Brand>

            <Nav>
            <Nav.Link as={Link} to="/trains">
              Trains
            </Nav.Link>
            </Nav>

           
            <Nav className="mr-auto">

               {window.localStorage['jwt'] ?
              <Nav.Link as={Link} to="/lines">Lines</Nav.Link> : 
                <Nav.Link as={Link} to="/"></Nav.Link>
              }
           

              {window.localStorage['jwt'] ?
              <Button hidden onClick = {()=>logout()}>Logout</Button> :
              <Nav.Link as={Link} to="/register">Registration</Nav.Link>
              }


              {window.localStorage['jwt']?
              <Button onClick = {()=>logout()}>Logout</Button> :
              <Nav.Link as ={Link} to="/login">Login</Nav.Link>
             }

             {window.localStorage['jwt'] ?
              <Nav.Link>logged in user : {window.localStorage['username']}</Nav.Link>:
              ""
            }

            {window.localStorage['jwt'] ?
            <Nav.Link as={Link} to='/user/reservations'>User Reservations</Nav.Link> :
            ""
            }

            </Nav>
            



          </Navbar>



          
           
          <Container style={{paddingTop: "25px"}}>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/lines" element={<Line/>}/>
              <Route path="/*" element={<NotFound/>}/>
              <Route path="/lines/add" element={<AddLine/>}/>
              <Route path="/lines/edit/:id" element={<EditLine/>}/>
              <Route path="/reservation/:id" element={<Reservation/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Registration/>}/>
              <Route path="/user/reservations" element={<UserReservations/>}/>
              <Route path="/trains" element={<Trains/>}/>
            </Routes>
            
          </Container>

        </Router>

      </div>
    );
  }
};

const Roote = ReactDOM.createRoot(document.getElementById('root'))
Roote.render(<App/>);
