import axios from 'axios';

const TrainAxios = axios.create({
    baseURL: 'http://localhost:8080/api',
    headers: {
        Authorization: `Bearer ${localStorage.getItem('jwt')}`,
    },
});

TrainAxios.interceptors.request.use(

    function success(config){
        const token = window.localStorage.getItem('jwt');
        if(token){
            config.headers['Authorization'] = 'Bearer ' + token;
        }
        return config;
    },

    function error(err){
        return Promise.reject(err);
    }
);


export default TrainAxios;



