import TrainAxios from "../apis/TrainAxios";
import jwt_decode from "jwt-decode";

export const login = async function(username, password){
    const data = {
        username: username,
        password: password
    }

    try{
        const ret = await TrainAxios.post('users/auth', data);
        
        const decoded = jwt_decode(ret.data);
        console.log(decoded.role.authority);

        const role = decoded.role?.authority || decoded.role || null; // Adjust based on backend format
        if (role) {
            window.localStorage.setItem('role', role); // Store role in localStorage
            console.log('Role set in localStorage:', role);
        } else {
            console.error('Role not found in token');
        }

        //window.localStorage.setItem('role', decoded.role.authority);
        
        window.localStorage.setItem('jwt', ret.data);
        window.localStorage.setItem('username', username);
        console.log(ret)
        alert('successifuly loged in');

    }catch(error){
        console.log('Login error',error);
        alert('Invalid credentials')
    }
    window.location.reload();
}

export const logout = function(){
        window.localStorage.removeItem('jwt');
        window.localStorage.removeItem('role');
        window.localStorage.removeItem('username');
        window.location.reload();
        window.location.replace('http://localhost:3000/login');
}
