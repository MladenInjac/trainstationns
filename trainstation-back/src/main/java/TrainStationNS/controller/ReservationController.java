package TrainStationNS.controller;

import TrainStationNS.model.Reservation;
import TrainStationNS.service.ReservationService;
import TrainStationNS.support.ReservationDtoToReservation;
import TrainStationNS.support.ReservationToReservationDto;
import TrainStationNS.web.dto.ReservationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/reservations", produces = MediaType.APPLICATION_JSON_VALUE)
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private ReservationDtoToReservation toReservation;

    @Autowired
    private ReservationToReservationDto toReservationDto;

    @PostMapping(consumes= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReservationDTO> create(@jakarta.validation.Valid @RequestBody ReservationDTO reservationDto){

        Reservation reservation = toReservation.convert(reservationDto);

        Reservation savedReservation = reservationService.save(reservation);

        return new ResponseEntity<>(toReservationDto.convert(savedReservation), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ReservationDTO> delete(@PathVariable Long id){

        Reservation deletedReservation = reservationService.delete(id);

        if(deletedReservation != null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


}
