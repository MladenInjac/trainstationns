package TrainStationNS.controller;

import TrainStationNS.model.Train;
import TrainStationNS.service.TrainService;
import TrainStationNS.support.TrainDtoToTrain;
import TrainStationNS.support.TrainToTrainDto;
import TrainStationNS.web.dto.TrainDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/api/trains", produces= MediaType.APPLICATION_JSON_VALUE)
public class TrainController {

    @Autowired
    TrainService trainService;

    @Autowired
    TrainToTrainDto toTrainDto;

    @Autowired
    TrainDtoToTrain toTrain;

    @GetMapping
    public ResponseEntity<List<TrainDTO>>getAll(){
        List<Train> trains = trainService.findAll();
        return new ResponseEntity<>(toTrainDto.convert(trains), HttpStatus.OK);

    }
    
    @GetMapping("/{id}")
    public ResponseEntity<TrainDTO>getOne(@PathVariable Long id){
    	Train oneTrain = trainService.findOne(id);
    	if(oneTrain != null) {
    		return new ResponseEntity<>(toTrainDto.convert(oneTrain), HttpStatus.OK);
    	}
    	return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    	

    @PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TrainDTO>create(@jakarta.validation.Valid @RequestBody TrainDTO trainDto){
        Train train = toTrain.convert(trainDto);

        Train savedTrain = trainService.save(train);
        return new ResponseEntity<>(toTrainDto.convert(savedTrain), HttpStatus.CREATED);

    }
}
