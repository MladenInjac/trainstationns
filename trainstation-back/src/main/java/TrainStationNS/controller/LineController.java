package TrainStationNS.controller;

import TrainStationNS.model.Line;
import TrainStationNS.service.LineService;
import TrainStationNS.support.LineDtoToLine;
import TrainStationNS.support.LineToLineDto;
import TrainStationNS.web.dto.LineDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/lines", produces = MediaType.APPLICATION_JSON_VALUE)
public class LineController {

    @Autowired
    private LineService lineService;

    @Autowired
    private LineToLineDto toLineDto;

    @Autowired
    private LineDtoToLine toLine;

    @GetMapping
    public ResponseEntity<List<LineDTO>>getAll(@RequestParam(defaultValue ="0") int pageNo){
        Page<Line> allLines = lineService.findAll(pageNo);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Total-Pages", allLines.getTotalPages() + "");

        return new ResponseEntity<>(toLineDto.convert(allLines.getContent()), headers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<LineDTO>get(@PathVariable Long id){
        Line oneLine = lineService.findOne(id);

        if(oneLine != null){
            return new ResponseEntity<>(toLineDto.convert(oneLine), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LineDTO>create(@Validated @RequestBody LineDTO lineDto){

        Line line = toLine.convert(lineDto);

        Line savedLine = lineService.save(line);
        return new ResponseEntity<>(toLineDto.convert(savedLine), HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value = "/{id}", consumes=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LineDTO> update(@PathVariable Long id, @Validated @RequestBody LineDTO lineDto){

        if(!id.equals(lineDto.getId())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Line line = toLine.convert(lineDto);
        Line changedLine = lineService.save(line);
        return new ResponseEntity<>(toLineDto.convert(changedLine), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void>delete(@PathVariable Long id){
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        System.out.println("Authenticated User: " + auth.getName());
        System.out.println("User Authorities: " + auth.getAuthorities());
        
        Line deletedLine = lineService.delete(id);

        if(deletedLine != null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT); 
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/search")
    public ResponseEntity<List<LineDTO>>search(
            @RequestParam(required = false) String destination,
            @RequestParam(required = false) Long trainId,
            @RequestParam(required = false) Double ticketPriceTo) {

        List<Line> rez = lineService.find(destination, trainId, ticketPriceTo);
        return new ResponseEntity<>(toLineDto.convert(rez), HttpStatus.OK);
    }


}
