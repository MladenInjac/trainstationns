package TrainStationNS.support;

import TrainStationNS.model.User;
import TrainStationNS.web.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


	@Component
	public class UserToUserDto implements Converter<User, UserDTO> {

		@Autowired
		private ReservationToReservationDto toResDto;

		@Override
		public UserDTO convert(User user) {

			UserDTO userDto = new UserDTO();

			userDto.setId(user.getId());
			userDto.setEMail(user.getEMail());
			userDto.setName(user.getName());
			userDto.setSurname(user.getSurname());
			userDto.setUsername(user.getUsername());

			if(user.getReservations() != null){
				userDto.setReservations(toResDto.convert(user.getReservations()));
			}


			return userDto;
		}

		public List<UserDTO> convert(List<User> users) {
			return users.stream()
					.map(this::convert)
					.collect(Collectors.toList());
		}
	}
