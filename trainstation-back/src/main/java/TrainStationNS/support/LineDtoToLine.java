package TrainStationNS.support;

import TrainStationNS.model.Line;
import TrainStationNS.service.LineService;
import TrainStationNS.service.TrainService;
import TrainStationNS.web.dto.LineDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class LineDtoToLine implements Converter<LineDTO, Line> {

    @Autowired
    LineService lineService;

    @Autowired
    TrainService trainService;

    @Override
    public Line convert(LineDTO source) {
        Line line;

        if(source.getId() == null){
            line = new Line();
        }else{
            line = lineService.findOne(source.getId());
        }
        if(line != null){
            line.setNumberOfSeats(source.getNumberOfSeats());
            line.setTicketPrice(source.getTicketPrice());
            line.setTime(source.getTime());
            line.setDestination(source.getDestination());
            if(source.getTrain() != null){
                line.setTrain(trainService.findOne(source.getTrain().getId()));
            }
        }
        return line;
    }
    
    public List<Line>convert(List<LineDTO>dtoList){
    	return dtoList.stream()
    			.map(this::convert)
    			.collect(Collectors.toList());
    }

}
