package TrainStationNS.support;

import TrainStationNS.model.User;
import TrainStationNS.web.dto.UserRes;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserRes implements Converter<User, UserRes> {
    @Override
    public UserRes convert(User source) {

        UserRes userRes = new UserRes();

        userRes.setId(source.getId());
        userRes.setEMail(source.getEMail());
        userRes.setUsername(source.getUsername());
        userRes.setName(source.getName());
        userRes.setSurname(source.getSurname());

        return userRes;
    }
}

