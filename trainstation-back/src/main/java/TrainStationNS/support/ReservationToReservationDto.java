package TrainStationNS.support;

import TrainStationNS.model.Reservation;
import TrainStationNS.model.User;
import TrainStationNS.service.UserService;
import TrainStationNS.web.dto.ReservationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ReservationToReservationDto implements Converter<Reservation, ReservationDTO> {

    @Autowired
    private LineToLineDto toLineDto;

    @Autowired
    private UserService userService;

    @Autowired
    private UserToUserRes toUserRes;

    @Override
    public ReservationDTO convert(Reservation source) {

        ReservationDTO dto = new ReservationDTO();

        dto.setId(source.getId());
        dto.setNumberOfPassengers(source.getNumberOfPassengers());
        dto.setDate(source.getDate());
        dto.setOnName(source.getOnName());
        dto.setLine(toLineDto.convert(source.getLine()));

        User user = userService.getOne(source.getUser().getId());
        dto.setUser(toUserRes.convert(user));

        return dto;
    }
    
    public List<ReservationDTO> convert(List<Reservation>reservations){
    	return reservations.stream()
    			.map(this::convert)
    			.collect(Collectors.toList());
    }
}
