package TrainStationNS.support;

import TrainStationNS.model.Train;
import TrainStationNS.service.TrainService;
import TrainStationNS.web.dto.TrainDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TrainDtoToTrain implements Converter<TrainDTO, Train> {

    @Autowired
    TrainService trainService;

    @Override
    public Train convert(TrainDTO source) {

        Train train;

        if(source.getId() == null){
            train = new Train();
        }else{
            train = trainService.findOne(source.getId());
        }

        if(train != null){
            train.setName(source.getName());
            train.setType(source.getType());
        }

        return train;
    }
}
