package TrainStationNS.support;

import TrainStationNS.model.Reservation;
import TrainStationNS.model.User;
import TrainStationNS.service.ReservationService;
import TrainStationNS.service.UserService;
import TrainStationNS.web.dto.ReservationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ReservationDtoToReservation implements Converter<ReservationDTO, Reservation> {

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private LineDtoToLine toLine;

    @Autowired
    private UserService userService;

    @Override
    public Reservation convert(ReservationDTO source) {

        Reservation reservation;

        if(source.getId() == null) {
            reservation = new Reservation();
        }else{
            reservation = reservationService.getOne(source.getId());
        }

        if(reservation != null){
            reservation.setNumberOfPassengers(source.getNumberOfPassengers());
            reservation.setDate(source.getDate());
            reservation.setOnName(source.getOnName());
            reservation.setLine(toLine.convert(source.getLine()));

            User user = userService.getOne(source.getUser().getId());

            reservation.setUser(user);
        }
        return reservation;
    }
        
        public List<Reservation> convert(List<ReservationDTO>dtos){
        	return dtos.stream()
        			.map(this::convert)
        			.collect(Collectors.toList());
        }
}
