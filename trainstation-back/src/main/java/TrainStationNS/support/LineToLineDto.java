package TrainStationNS.support;

import TrainStationNS.model.Line;
import TrainStationNS.web.dto.LineDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class LineToLineDto implements Converter<Line, LineDTO> {

    @Autowired
    TrainToTrainDto toTrainDto;

    @Override
    public LineDTO convert(Line source) {

        LineDTO dto = new LineDTO();

        dto.setId(source.getId());
        dto.setNumberOfSeats(source.getNumberOfSeats());
        dto.setTicketPrice(source.getTicketPrice());
        dto.setTime(source.getTime());
        dto.setDestination(source.getDestination());
        dto.setTrain(toTrainDto.convert(source.getTrain()));

        return dto;

    }

    public List<LineDTO>convert(List<Line>lines){
    	return lines.stream()
    			.map(this::convert)
    			.collect(Collectors.toList());
    }
}
