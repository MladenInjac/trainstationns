package TrainStationNS.support;

import TrainStationNS.model.User;
import TrainStationNS.service.UserService;
import TrainStationNS.web.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

	@Component
	public class UserDtoToUser implements Converter<UserDTO, User> {

	    @Autowired
	    private UserService userService;

		@Override
		public User convert(UserDTO userDto) {
			User user = null;
			if(userDto.getId() != null){
				user = userService.findOne(userDto.getId()).get();
			}
			if(user == null){
				user = new User();
			}

			user.setUsername(userDto.getUsername());
			user.setEMail(userDto.getEMail());
			user.setName(userDto.getName());
			user.setSurname(userDto.getSurname());

			return user;
		}
	}

