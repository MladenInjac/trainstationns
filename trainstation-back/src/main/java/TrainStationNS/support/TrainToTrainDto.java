package TrainStationNS.support;

import TrainStationNS.model.Train;
import TrainStationNS.web.dto.TrainDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TrainToTrainDto  implements Converter<Train, TrainDTO> {


    @Override
    public TrainDTO convert(Train source) {

        TrainDTO trainDto = new TrainDTO();

        trainDto.setId(source.getId());
        trainDto.setName(source.getName());
        trainDto.setType(source.getType());

        return trainDto;
    }

    public List<TrainDTO> convert(List<Train>trains){
        return trains.stream()
        		.map(this::convert)
        		.collect(Collectors.toList());
    }
}
