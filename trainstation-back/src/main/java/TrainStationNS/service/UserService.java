package TrainStationNS.service;


import TrainStationNS.model.User;

import java.util.Optional;

public interface UserService {

    Optional<User> findOne(Long id);

    Optional<User> findbyUsername(String username);

    User save(User user);

    User findByUsername(String username);

    User getOne(Long id);

    /**
    List<User> findAll();

    Page<User> findAll(int pageNo);



    void delete(Long id);*/



}
