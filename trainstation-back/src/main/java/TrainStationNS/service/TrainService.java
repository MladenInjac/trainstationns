package TrainStationNS.service;

import TrainStationNS.model.Train;

import java.util.List;

public interface TrainService {

    List<Train> findAll();

    Train save(Train train);

    Train findOne(Long id);

}

