package TrainStationNS.service;

import TrainStationNS.model.Reservation;

public interface ReservationService {

    Reservation delete(Long id);

    Reservation save (Reservation reservation);

    Reservation getOne(Long id);


}

