package TrainStationNS.service;

import TrainStationNS.model.Line;
import org.springframework.data.domain.Page;

import java.util.List;

public interface LineService {

    Page<Line> findAll(int pageNo);

    Line findOne(Long id);

    Line save(Line line);

    Line update (Line line);

    Line delete (Long id);

    List<Line> find(String destination, Long trainId, Double ticketPriceTo);
}

