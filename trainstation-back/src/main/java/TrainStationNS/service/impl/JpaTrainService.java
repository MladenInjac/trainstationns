package TrainStationNS.service.impl;

import TrainStationNS.model.Train;
import TrainStationNS.repository.TrainRepository;
import TrainStationNS.service.TrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JpaTrainService implements TrainService {

    @Autowired
    TrainRepository trainRepository;

    @Override
    public List<Train> findAll() {
        return trainRepository.findAll();
    }

    @Override
    public Train save(Train train) {
        return trainRepository.save(train);
    }

    @Override
    public Train findOne(Long id) {
        return trainRepository.findOneById(id);
    }
}

