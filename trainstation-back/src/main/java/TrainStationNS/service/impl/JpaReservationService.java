package TrainStationNS.service.impl;

import TrainStationNS.model.Line;
import TrainStationNS.model.Reservation;
import TrainStationNS.repository.ReservationRepository;
import TrainStationNS.service.LineService;
import TrainStationNS.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class JpaReservationService implements ReservationService {

    @Autowired
    private LineService lineService;

    @Autowired
    private ReservationRepository reservationRepository;



    @Override
    public Reservation delete(Long id) {
        Optional<Reservation> res = reservationRepository.findById(id);

        if(res.isPresent()){
            reservationRepository.deleteById(id);
            return res.get();
        }
        return null;
    }

    @Override
    public Reservation save(Reservation reservation) {

        int numberOfSeats = reservation.getLine().getNumberOfSeats();
        int numberOfPassengers = reservation.getNumberOfPassengers();

        if(numberOfSeats >= numberOfPassengers) {
            Line line = reservation.getLine();
            line.setNumberOfSeats(line.getNumberOfSeats() - reservation.getNumberOfPassengers());
            lineService.update(line);

            return reservationRepository.save(reservation);
        }
        return null;
    }



    @Override
    public Reservation getOne(Long id) {
        return null;
    }


}
