package TrainStationNS.service.impl;

import TrainStationNS.model.Line;
import TrainStationNS.repository.LineRepository;
import TrainStationNS.service.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JpaLineService implements LineService {

    @Autowired
    LineRepository lineRepository;

    @Override
    public Page<Line> findAll(int pageNo) {
        return lineRepository.findAll(PageRequest.of(pageNo, 5));
    }

    @Override
    public Line findOne(Long id) {
        return lineRepository.findOneById(id);
    }

    @Override
    public Line save(Line line) {
        return lineRepository.save(line);
    }

    @Override
    public Line update(Line line) {

        return lineRepository.save(line);
    }

    @Override
    public Line delete(Long id) {
        Optional<Line> lines = lineRepository.findById(id);
        if(lines.isPresent()) {
            lineRepository.deleteById(id);
            return lines.get();
        }
        return null;
    }

    @Override
    public List<Line> find(String destination, Long trainId, Double ticketPriceTo) {

        if(destination == null) {
            destination = "";
        }
        if(ticketPriceTo == null) {
            ticketPriceTo = Double.MAX_VALUE;
        }
        if(trainId == null) {
            return lineRepository.findByDestinationIgnoreCaseContainsAndTicketPriceLessThanEqual(destination, ticketPriceTo);
        }
        return lineRepository.findByDestinationIgnoreCaseContainsAndTrainIdAndTicketPriceLessThanEqual(destination, trainId, ticketPriceTo);
    }


}

