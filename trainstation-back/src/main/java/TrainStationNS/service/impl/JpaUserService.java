package TrainStationNS.service.impl;

import TrainStationNS.enumeration.UserRole;

import TrainStationNS.model.User;
import TrainStationNS.repository.UserRepository;
import TrainStationNS.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class JpaUserService implements UserService {

    @Autowired
    private UserRepository userRepository;

	
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public JpaUserService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<User> findOne(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User save(User user) {
        user.setRole(UserRole.USER);
        return userRepository.save(user);
    }

    @Override
    public Optional<User> findbyUsername(String username) {

        return userRepository.findFirstByUsername(username);
    }


    public User findByUsername(String username) {

        return userRepository.findByUsername(username);
    }

    @Override
    public User getOne(Long id) {
        return userRepository.getReferenceById(id);
    }

}