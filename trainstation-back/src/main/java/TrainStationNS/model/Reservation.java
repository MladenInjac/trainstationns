package TrainStationNS.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Reservation {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column
    private int numberOfPassengers;

    @Column
    private String date;

    @Column
    @NotNull(message="Name can not be null")
    private String onName;

    @ManyToOne
    private Line line;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;
    
}
