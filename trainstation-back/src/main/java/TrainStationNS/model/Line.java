package TrainStationNS.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Line {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private int numberOfSeats;

    @Column
    private double ticketPrice;

    @Column
    private String time;

    @Column(nullable = false)
    private String destination;

    @ManyToOne
    private Train train;

    @OneToMany(mappedBy = "line", cascade = CascadeType.ALL)
    private List<Reservation> reservations;

}
