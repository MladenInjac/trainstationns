package TrainStationNS.web.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthUserDto {

    private String username;
    private String password;
}
