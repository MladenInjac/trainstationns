package TrainStationNS.web.dto;


import jakarta.validation.constraints.Email;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class UserDTO {

	    private Long id;

	    @jakarta.validation.constraints.NotBlank
	    private String username;

	    @jakarta.validation.constraints.NotEmpty
	    @Email
	    private String eMail;

	    @jakarta.validation.constraints.Size(min=3, max=50)
	    private String name;

	    @jakarta.validation.constraints.Size(min=3, max=50)
	    private String surname;

		private List<ReservationDTO> reservations;

}


