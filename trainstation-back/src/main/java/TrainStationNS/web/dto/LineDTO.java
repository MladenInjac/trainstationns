package TrainStationNS.web.dto;

import jakarta.validation.constraints.Positive;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LineDTO {

    @Positive
    private Long id;

    private int numberOfSeats;

    private double ticketPrice;

    private String time;

    private String destination;

    private TrainDTO train;

}
