package TrainStationNS.web.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserRes {

    private Long id;

    private String username;

    private String eMail;

    private String name;

    private String surname;
}
