package TrainStationNS.web.dto;

import lombok.Data;

@Data
public class UserRegistrationDTO extends UserDTO{

    private String password;

    private String repeatedPassword;
}
