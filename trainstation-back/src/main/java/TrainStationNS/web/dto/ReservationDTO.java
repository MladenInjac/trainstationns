package TrainStationNS.web.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ReservationDTO {

    private Long id;

    private int numberOfPassengers;

    private String date;

    private String onName;

    private LineDTO line;

    private UserRes user;
}
