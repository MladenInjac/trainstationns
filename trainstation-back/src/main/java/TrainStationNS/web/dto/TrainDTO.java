package TrainStationNS.web.dto;

import jakarta.validation.constraints.Positive;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TrainDTO {

    @Positive
    private Long id;

    private String name;

    private String type;

}
