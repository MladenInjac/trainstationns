package TrainStationNS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "TrainStationNS")
@EnableJpaRepositories(basePackages = "TrainStationNS.repository")
@EntityScan(basePackages = "TrainStationNS.model")
public class TrainStationNsApplication extends SpringBootServletInitializer {


	public static void main(String[] args) {

		SpringApplication.run(TrainStationNsApplication.class, args);
		
	}
	
}
