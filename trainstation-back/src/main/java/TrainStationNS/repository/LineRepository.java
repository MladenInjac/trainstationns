package TrainStationNS.repository;

import TrainStationNS.model.Line;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LineRepository extends JpaRepository<Line, Long> {

    Line findOneById(Long id);

    List<Line> findByDestinationIgnoreCaseContainsAndTrainId(String destination, Long trainId);

    List<Line> findByDestinationIgnoreCaseContainsAndTicketPriceLessThanEqual(String destination, Double ticketPriceTo);

    List<Line> findByDestinationIgnoreCaseContainsAndTrainIdAndTicketPriceLessThanEqual(String destination, Long trainId, Double ticketPriceTo );


}
