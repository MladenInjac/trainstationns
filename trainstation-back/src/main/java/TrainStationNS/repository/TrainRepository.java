package TrainStationNS.repository;

import TrainStationNS.model.Train;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainRepository extends JpaRepository<Train, Long> {

    Train findOneById(Long id);
}

