package TrainStationNS.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import io.jsonwebtoken.security.Keys;
import java.security.Key;

@Component
public class TokenUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(TokenUtils.class);

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Value("${jwt.expiration}")
    private Long expiration;

    private Key getSigningKey() {
    	byte[] keyBytes = Decoders.BASE64.decode(jwtSecret);
        return Keys.hmacShaKeyFor(keyBytes);
        // Automatically generate a 512-bit key for HS512
        //return Keys.secretKeyFor(SignatureAlgorithm.HS512);
    }

    public String getUsernameFromToken(String token) {
        try {
            Claims claims = this.getClaimsFromToken(token);
            return claims.getSubject();
        } catch (Exception e) {
            return null;
        }
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            JwtParserBuilder parserBuilder = Jwts.parserBuilder();
            parserBuilder.setSigningKey(getSigningKey());
            claims = parserBuilder.build().parseClaimsJws(token).getBody();
        } catch (JwtException e) {
            logger.error("Invalid JWT token: " + e.getMessage());
            return null;
        }
        return claims;
    }

    public Date getExpirationDateFromToken(String token) {
        try {
            final Claims claims = this.getClaimsFromToken(token);
            return claims.getExpiration();
        } catch (Exception e) {
            return null;
        }
    }

    private boolean isTokenExpired(String token) {
        final Date expirationDate = this.getExpirationDateFromToken(token);
        return expirationDate != null && expirationDate.before(new Date(System.currentTimeMillis()));
    }

    public boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return username != null && username.equals(userDetails.getUsername()) && !isTokenExpired(token);
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", userDetails.getUsername());
        String roles = userDetails.getAuthorities().stream()
        		.map(grantedAuthority -> grantedAuthority.getAuthority())
        		.collect(Collectors.joining(","));
        claims.put("role", roles);
        claims.put("created", new Date(System.currentTimeMillis()));

        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + expiration * 1000))
                .signWith(getSigningKey(), SignatureAlgorithm.HS512)
                .compact();
    }
}
