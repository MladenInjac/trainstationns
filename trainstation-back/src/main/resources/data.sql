INSERT INTO user (id, e_mail, username, password, name, surname, role)
              VALUES (3,'mladja@gmail.com','mladen','$2a$10$YYOicddq/3gEAzp.pee4H.CCRLhRnUXq.SWCrNDqrvZDStuf2652K','Mladen','Injac','ADMIN');

INSERT INTO train (id, name, type) VALUES (1, 'Soko', 'interCity');
INSERT INTO train (id, name, type) VALUES (2, 'SerbiaTrain', 'international');
INSERT INTO train (id, name, type) VALUES (3, 'RegioTrain', 'regional');
INSERT INTO train (id, name, type) VALUES (4, 'Nostalgic', 'local');

INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (1, 150, 380, '08:00', 'Novi Sad', 1);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (2, 140, 380, '08:30', 'Beograd', 1);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (3, 80, 400, '10:15', 'Subotica', 1);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (4, 120, 780, '09:20', 'Nis', 3);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (5, 90, 850, '11:00', 'Leskovac', 3);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (6, 60, 640, '11:40', 'Uzice', 3);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (7, 77, 780, '12:45', 'Kosovska Mitrovica', 3);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (8, 130, 1090, '22:30', 'Bar', 2);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (9, 200, 2000, '21:45', 'Budapest', 2);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (10, 220, 2680, '23:05', 'Prague', 2);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (11, 230, 2900, '20:50', 'Wiena', 2);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (12, 190, 2060, '19:30', 'Ljubljana', 2);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (13, 70, 2560, '18:15', 'Sofija', 2);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (14, 159, 1500, '18:30', 'Banja Luka', 2);
INSERT INTO line (id, number_of_seats, ticket_price, time, destination, train_id )
VALUES (15, 40, 440, '14:00', 'Mokra Gora', 4);
